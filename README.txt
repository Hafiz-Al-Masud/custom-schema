>> This is a custom schematic template. It will create a bunch of folders & files which are customizable.

>> To use this at first run the following commands inside your project:
$ npm i
$ npm i angular-crud

>> Then replace all the files inside "\project-name\node_modules\angular-crud\src\crud-module\files" directory with the "__name@dasherize__" folder in this repository.

>> You should also modify the "\project-name\node_modules\@angular-devkit\core\src\utils\strings.js" file in the following manner:

>> Open the "strings.js" file with a text editor.
insert the following function:

/**
 More general than decamelize. Returns the upper\_case\_and\_underscored
 form of a string.

 ```javascript
 'innerHTML'.underscore();          // 'INNER_HTML'
 'action_name'.underscore();        // 'ACTION_NAME'
 'css-class-name'.underscore();     // 'CSS_CLASS_NAME'
 'my favorite items'.underscore();  // 'MY_FAVORITE_ITEMS'
 ```

 @method upperCaseUnderScore
 @param {String} str The string to uppercase & underscore.
 @return {String} the uppercase & underscored string.
 */
function upperCaseUnderScore(str) {
    return str
        .replace(STRING_UNDERSCORE_REGEXP_1, '$1_$2')
        .replace(STRING_UNDERSCORE_REGEXP_2, '_')
        .toUpperCase();
}
exports.upperCaseUnderScore = upperCaseUnderScore;

>> We used the following codes as placeholders:

For file/folder names: 
__name@dasherize__ (file-name)
For file contents:
<%=classify(name)%> (FileName)
<%=dasherize(name)%> (file-name)
<%=camelize(name)%> (fileName)
<%=upperCaseUnderScore(name)%> (FILE_NAME)

>> We will now add a folder in a name as you want (let the name is "my-schema") inside "\src\app" folder.
We should add a empty json file named as "model.json" in the same folder. Just put "{}" inside the json file.

>> Now change the directory into: \project-name\src\app\my-schema

>> run the CLI command:
$ ng generate angular-crud:crud-module my-library-name

>> Now you should see the generated files inside: "\project-name\src\app\my-schema"