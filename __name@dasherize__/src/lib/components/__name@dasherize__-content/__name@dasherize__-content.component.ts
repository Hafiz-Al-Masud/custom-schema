import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: '<%=dasherize(name)%>-content',
  templateUrl: './<%=dasherize(name)%>-content.component.html',
  styleUrls: ['./<%=dasherize(name)%>-content.component.scss']
})
export class <%=classify(name)%>ContentComponent implements OnInit, OnDestroy {

  unSubscribe$ = new Subject();

  constructor() {

  }


  ngOnInit () {

  }


  ngOnDestroy (): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }
}
