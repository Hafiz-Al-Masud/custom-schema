import { Component, OnInit } from '@angular/core';

@Component({
  selector: '<%=dasherize(name)%>-content-toolbar',
  templateUrl: './<%=dasherize(name)%>-content-toolbar.component.html',
  styleUrls: ['./<%=dasherize(name)%>-content-toolbar.component.scss']
})
export class <%=classify(name)%>ContentToolbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
