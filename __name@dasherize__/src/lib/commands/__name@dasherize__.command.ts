import { CommandBase } from '@orbitax/orbitax-command';
import { <%=classify(name)%>Config } from '../<%=dasherize(name)%>.config';

export class Delete<%=classify(name)%>Command extends CommandBase {

  constructor () {
    super('');
  }
}
