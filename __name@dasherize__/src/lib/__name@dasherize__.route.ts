import {Routes} from '@angular/router';
import { <%=classify(name)%>ContentComponent } from './components/<%=dasherize(name)%>-content/<%=dasherize(name)%>-content.component';
import { <%=classify(name)%>ContentToolbarComponent } from './components/<%=dasherize(name)%>-content-toolbar/<%=dasherize(name)%>-content-toolbar.component';


export const <%=camelize(name)%>Routes: Routes = [
  {
    path      : '<%=dasherize(name)%>',
    component: <%=classify(name)%>ContentComponent
  },
  {
    path: '<%=dasherize(name)%>-toolbar',
    component: <%=classify(name)%>ContentToolbarComponent,
    outlet: 'top-toolbar'
  }
];
