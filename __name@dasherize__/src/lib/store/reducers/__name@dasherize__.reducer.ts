import {FieldSection, PagedResponse, PagedResponseRow, PageInfo} from '@orbitax/orbitax-particle';
import {
  <%=classify(name)%>Actions, GET_<%=upperCaseUnderScore(name)%>_SUCCESS,
  Get<%=classify(name)%>SuccessAction
} from '../actions/<%=dasherize(name)%>.action';

export interface <%=classify(name)%>State {
  pagedResponse: PagedResponse;
  fieldSections: FieldSection[];
  pageInfo: PageInfo;
  currentAction: string;
  loading: boolean;
  loaded: boolean;
  currentItem: PagedResponseRow;
  fromDate: Date;
  toDate: Date;
}

export const initialState = <<%=classify(name)%>State> {
  pagedResponse : <PagedResponse>{
    items: [],
    pageCount: 0,
    pageNo: 0,
    pageSize: 10,
    recordCount: 0
  },
  pageInfo: {
    pageCount: 0,
    pageNo: 0,
    pageSize: 10,
    recordCount: 0
  }
};

export function <%=camelize(name)%>Reducer(state: <%=classify(name)%>State = initialState, action: <%=classify(name)%>Actions ) {
  switch (action.type) {
    case GET_<%=upperCaseUnderScore(name)%>_SUCCESS: {
      const pagedResponse = (action as Get<%=classify(name)%>SuccessAction).pagedResponse;
      const pageInfo = {
        ...state.pageInfo,
        pageNo: pagedResponse.pageNo,
        pageCount: pagedResponse.pageCount,
        pageSize: pagedResponse.pageSize,
        recordCount: pagedResponse.recordCount
      }
      return {
        ...state, pagedResponse: pagedResponse, pageInfo: pageInfo
      };
    }
  }
  return state;
}


