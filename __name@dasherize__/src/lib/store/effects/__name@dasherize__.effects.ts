import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {catchError, map, mergeMap} from 'rxjs/internal/operators';
import {of} from 'rxjs/index';
import { SnackBarService} from '@orbitax/orbitax-particle';
import {CommandService} from '@orbitax/orbitax-command';
import {
  GET_<%=upperCaseUnderScore(name)%>, Get<%=classify(name)%>Action, Get<%=classify(name)%>FailedAction, Get<%=classify(name)%>SuccessAction
} from '../actions/<%=dasherize(name)%>.action';
import { <%=classify(name)%>ApiService } from 'projects/<%=dasherize(name)%>/src/lib/services/<%=dasherize(name)%>.service';
import { <%=classify(name)%>State } from '../reducers/<%=dasherize(name)%>.reducer';
import { Store } from '@ngrx/store';
import 'rxjs/add/operator/withLatestFrom';
@Injectable()
export class <%=classify(name)%>Effect {
  constructor(private actions$: Actions,
              private <%=camelize(name)%>ApiService: <%=classify(name)%>ApiService,
              private snackBarService: SnackBarService,
              private commandService: CommandService,
              private store$: Store<<%=classify(name)%>State>) {
  }

  @Effect()
  Get<%=classify(name)%> = this.actions$
    .ofType(GET_<%=upperCaseUnderScore(name)%>)
    .pipe(
      mergeMap((action: Get<%=classify(name)%>Action) => {
        return this.<%=camelize(name)%>ApiService.get<%=classify(name)%>().pipe(
          map((payload: any) => {
            return new Get<%=classify(name)%>SuccessAction(payload);
          }),
          catchError(error => of(new Get<%=classify(name)%>FailedAction(error)))
        );
      })
    );
}
