import {Action} from '@ngrx/store';
import { PagedResponse} from '@orbitax/orbitax-particle';

export const GET_<%=upperCaseUnderScore(name)%> = '[<%=classify(name)%>] GET_<%=upperCaseUnderScore(name)%>';
export const GET_<%=upperCaseUnderScore(name)%>_SUCCESS = '[<%=classify(name)%>] GET_<%=upperCaseUnderScore(name)%>_SUCCESS';
export const GET_<%=upperCaseUnderScore(name)%>_FAILED = '[<%=classify(name)%>] GET_<%=upperCaseUnderScore(name)%>_FAILED';




export class Get<%=classify(name)%>Action implements Action {
  readonly type = GET_<%=upperCaseUnderScore(name)%>;

  constructor(public fromDate: string, public toDate: string, public pageNo: number, public pageSize: number) {
  }
}

export class Get<%=classify(name)%>SuccessAction implements Action {
  readonly type = GET_<%=upperCaseUnderScore(name)%>_SUCCESS;

  constructor(public pagedResponse: PagedResponse) {
  }
}

export class Get<%=classify(name)%>FailedAction implements Action {
  readonly type = GET_<%=upperCaseUnderScore(name)%>_FAILED;

  constructor(public payload: any) {
    console.error(payload);
  }
}

export type <%=classify(name)%>Actions = Get<%=classify(name)%>Action
  | Get<%=classify(name)%>SuccessAction
  | Get<%=classify(name)%>FailedAction;
