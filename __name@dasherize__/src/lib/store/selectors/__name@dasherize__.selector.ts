import { createFeatureSelector, createSelector } from '@ngrx/store';
import { <%=classify(name)%>State } from '../../store/reducers/<%=dasherize(name)%>.reducer';

export const get<%=classify(name)%>State = createFeatureSelector<<%=classify(name)%>State>(
  '<%=dasherize(name)%>'
);
export const get<%=classify(name)%>PagedResponse = createSelector(
  get<%=classify(name)%>State,
  (state: <%=classify(name)%>State) => state.pagedResponse
);
