import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable, of} from 'rxjs/index';
import { <%=classify(name)%>Config } from 'projects/<%=dasherize(name)%>/src/lib/<%=dasherize(name)%>.config';

@Injectable()
export class <%=classify(name)%>ApiService {

  constructor(private httpClient: HttpClient) {
  }

  get<%=classify(name)%>(fromDate: string, toDate: string, pageNo: number, pageSize: number): Observable<any> {
    const url = <%=classify(name)%>Config.get<%=classify(name)%>();
    return this.httpClient.get<any>(url);
  }

}


