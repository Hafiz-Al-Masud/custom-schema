/**
 * Created by zinia on 11/8/2018.
 */
// @dynamic
export class <%=classify(name)%>Config {
  static get <%=classify(name)%>API (): string {
    return 'http://localhost:15796/api/';
  }


  static get<%=classify(name)%> (): string {
    return `${<%=classify(name)%>Config.<%=classify(name)%>API}`;
  }


}

