import { RouterModule } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import {
  MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatDividerModule, MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule, MatNativeDateModule, MatOptionModule, MatPaginatorModule, MatProgressSpinnerModule, MatRadioModule,
  MatRippleModule, MatSelectModule, MatSidenavModule, MatStepperModule, MatTableModule, MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import {StoreModule} from '@ngrx/store';
import {FormsModule} from '@angular/forms';
import {EffectsModule} from '@ngrx/effects';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfigurationModule} from '@orbitax/orbitax-configuration';
import {FuseConfirmDialogModule, FuseSearchBarModule, FuseSharedModule} from '@orbitax/orbitax-barebone-fuse';
import {ParticleModule} from '@orbitax/orbitax-particle';
import { EntityReportsContentComponent } from './components/<%=dasherize(name)%>-content/<%=dasherize(name)%>-content.component';
import { EntityReportsContentToolbarComponent } from './components/<%=dasherize(name)%>-content-toolbar/<%=dasherize(name)%>-content-toolbar.component';
import { entityReportsReducer } from './store/reducers/<%=dasherize(name)%>.reducer';
import { EntityReportsEffect } from './store/effects/<%=dasherize(name)%>.effects';
import { entityReportsRoutes } from './<%=dasherize(name)%>.route';
import { EntityReportsApiService } from './services/<%=dasherize(name)%>.service';



@NgModule({
  declarations   : [
    <%=classify(name)%>ContentComponent,
    <%=classify(name)%>ContentToolbarComponent
  ],
  imports        : [
    ConfigurationModule,
    RouterModule.forChild(<%=camelize(name)%>Routes),
    StoreModule.forFeature('<%=dasherize(name)%>', <%=camelize(name)%>Reducer),
    EffectsModule.forFeature([<%=classify(name)%>Effect]),
    CdkTableModule,
    MatButtonModule,
    MatCheckboxModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatRippleModule,
    MatSidenavModule,
    MatTableModule,
    MatToolbarModule,
    FuseSharedModule,
    FuseConfirmDialogModule,
    MatOptionModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    FuseSearchBarModule,
    MatDividerModule,
    MatSelectModule,
    MatExpansionModule,
    FormsModule,
    ParticleModule,
    MatDialogModule,
    MatStepperModule,
    MatTooltipModule,
    MatRadioModule,
    CommonModule,
    MatButtonModule
  ],
  providers      : [<%=classify(name)%>ApiService]
})
export class <%=classify(name)%>Module { }
