/*
 * Public API Surface of <%=dasherize(name)%>
 */

export * from './lib/services/<%=dasherize(name)%>.service';
export * from './lib/<%=dasherize(name)%>.config';
export * from './lib/<%=dasherize(name)%>.module';
